require 'rails_helper'

feature 'Creating Posts' do
  scenario 'can create job' do
    # Visit root route (index)
    visit '/'
    # Click create post link
    click_link 'Create Post'
    # Fill in the form with needed information (itle and caption)
    fill_in 'Title', with: 'title'
    fill_in 'Caption', with: 'caption'
    # Click the submit button
    click_button 'Create Post'
    # Expect Page have content we submitted
    expect(page).to have_content('title')
    expect(page).to have_content('caption')
  end
end
