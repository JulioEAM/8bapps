Rails.application.routes.draw do
  devise_for :users
  resources :pics do
    member do
      put "like", to: "pics#upvote"
      get "like", to: "pics#upvote"
    end
  end
  root "pics#index"
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
end
